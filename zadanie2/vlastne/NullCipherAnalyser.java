package zadanie2.vlastne;

import java.util.List;
import zadanie2.nemenit.CipherAnalyser;


public class NullCipherAnalyser extends CipherAnalyser {

    public NullCipherAnalyser() {
        super();
    }

    @Override
    public void analyse(String input/*TSA*/) {
        List<String> candidates;
        // TODO NAIMPLEMENTOVAT METODU
        ScoreCalc sc = new ScoreCalc("zadanie2/vlastne/dictionary.txt");
        Node n = Node.loadDictionary(sc.getDict());

        //System.out.println(input);

        candidates = BruteForce.bruteForce(input);

        for (String cand : candidates) {
            //Double score = sc.rankText(cand);
            Double score = n.evaluate(cand, 3, 8);
            //System.out.println("##" + score + " - " + cand);
            super.result.put(cand, score);
        }
    }

   /*
    *  TU MOZETE ZADEFINOVAT/NAIMPLEMENTOVAT VLASTNE METODY
    */
}
