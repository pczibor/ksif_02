package zadanie2.vlastne;

import zadanie2.nemenit.CipherAnalyser;

import java.util.*;

public class CombinedCipherAnalyser extends CipherAnalyser {

    String vigenereKey;

    public CombinedCipherAnalyser(String vigenereKey) {
        if (vigenereKey.length() == 4) {
            // len v pripade, ze mam vsetky 4 OT (spravne OT :-))
            this.vigenereKey = vigenereKey;
        }
    }

    @Override
    public void analyse(String input) {
        // TODO NAIMPLEMENTOVAT METODU
        ScoreCalc sc = new ScoreCalc("zadanie2/vlastne/dictionary.txt");
        Node n = Node.loadDictionary(sc.getDict());
        // ked ste nasli spravne OT1 az OT4, tak viete vigenereKey
        if(vigenereKey == null) {
            return;
        }

        //VigenerePT vg = new VigenerePT(vigenereKey);
        String vig_decrypted = vgdecrypt(input, vigenereKey);

        Integer[] intarr = {0,1,2,3,4,5,6,7};
        List<Integer> intList = Arrays.asList(intarr);

        Double max = 0.0;
        for(int i=0;i<99990;++i) { // fuj
            String candidate = transpodecrypt(vig_decrypted, convertIntegers(intList));
            max = n.evaluate(candidate, 3, 8);
            super.result.put(candidate, max);
            Collections.shuffle(intList);
        }

    }

    public static int[] convertIntegers(List<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().intValue();
        }
        return ret;
    }

    public String transpodecrypt(String input, int[] key) {
        int columns = key.length;
        int rows = input.length() / columns;

        List<Integer> permList = new ArrayList<>();
        for (int c = 0; c < key.length; c++) {
            permList.add(key[c]);
        }

        char[][] matrix = new char[rows][columns];
        int idx = 0;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                matrix[r][permList.get(c)] = input.charAt(idx++);
            }
        }

        StringBuilder retVal = new StringBuilder();
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                if (matrix[r][c] != '\u0000') {
                    retVal.append(matrix[r][c]);
                }
            }
        }
        return retVal.toString();
    }

    public String vgdecrypt(String ct, String key) {

        StringBuilder pt = new StringBuilder();
        for (int i = 0; i < ct.length(); i++) {
            char k;

            k = key.charAt(i % key.length());
            int inNum = (ct.charAt(i) - 'a');
            inNum -= k - 'a';
            inNum += 26; // possible problem with negative modulo
            inNum %= 26;
            char inChar = (char) (inNum + 'a');
            pt.append(inChar);
        }
        return pt.toString();
    }

}
