package zadanie2.vlastne;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ScoreCalc {
    private ArrayList<String> dict;
    private String dictpath;
    final Double increment = 10.0;

    public ScoreCalc(ArrayList<String> dict) {
        this.dict = dict;
    }

    public ScoreCalc(String dictpath) {
        this.dictpath = dictpath;
        loadDict();
    }

    public ArrayList<String> getDict() {
        return dict;
    }

    public Double rankText(String input) {
        Double result = 0.0;
        for (String s : dict) {

            //int count = countAll(input, s);
            //result += count * increment;

            int r = input.indexOf(s);

            if (r > 0)
                result += increment;
        }

        return result;
    }

    private int countAll(String s, String sub_s) {
        int ret;
        int index = 0;
        int count = 0;

        while ((ret = s.indexOf(sub_s, index)) != -1) {
            ++count;
            index = ret + 1;
        }

        return count;
    }

    private void loadDict() {
        dict = new ArrayList<>();
        BufferedReader reader;

        try {
            reader = new BufferedReader(new FileReader(
                    dictpath
            ));
            String line = reader.readLine();
            while(line != null) {
                dict.add(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> loadDict(String path) {
        ArrayList<String> dict = new ArrayList<>();
        BufferedReader reader;

        try {
            reader = new BufferedReader(new FileReader(
                    path
            ));
            String line = reader.readLine();
            while(line != null) {
                dict.add(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dict;
    }


}
