package zadanie2.vlastne;

import java.util.ArrayList;
import java.util.List;

public class BruteForce {


    public static int guessBlockSize(String input) {
        int bsize = 0;
        String [] words = input.split(" ", 8);
        for (String word : words) {
            if (bsize < word.length())
                bsize = word.length();
        }

        return bsize;
    }

    public static String insertSpaces(String input, int num) {
        StringBuilder result = new StringBuilder();
        int add = 0;
        for (int i = 0; i < input.length(); i++) {
            if (add == num) {
                result.append(" ");
                add = 0;
            }
            result.append(input.charAt(i));
            ++add;
        }

        return result.toString();
    }

    public static List<String> bruteForce(String input) {
        String orig_input = input;
        ArrayList<String> dictionary = ScoreCalc.loadDict("zadanie2/vlastne/dictionary.txt");
        List<String> result = new ArrayList<>();
        input = input.replaceAll("\\s+", "");

        // A & B
        for (int bsize=3; bsize<8; ++bsize) {
            String _input = insertSpaces(input, bsize);
            //System.out.println(_input);

            String[] words = _input.split(" ");
            for (int i = 0; i < bsize; ++i) { // we try all positions
                StringBuilder candidate = new StringBuilder();
                for (String word : words) {
                    try {
                        candidate.append(word.charAt(i));
                    } catch (StringIndexOutOfBoundsException e) {
                        continue;
                    }

                }
                result.add(candidate.toString());
                result.add(candidate.reverse().toString());
            }
        }

        // D
        String [] words = orig_input.split(" ");
        StringBuilder candidate = new StringBuilder();
        for (String word : words) {
            candidate.append(word.charAt(0));
        }
        result.add(candidate.toString());
        //System.out.println("res: " + candidate.toString());



        // C
        for (int bsize = 3; bsize < 8; ++bsize) {
            String _input = insertSpaces(input, bsize);
            words = _input.split(" ");
            candidate = new StringBuilder();

            int index = 0;
            for (String word : words) {
                if (index == bsize) {
                    index = 0;
                }
                try {
                    candidate.append(word.charAt(index));
                } catch (StringIndexOutOfBoundsException e) {
                    ++index;
                    continue;
                }
                ++index;
            }
            result.add(candidate.toString());
        }


        //for (String s : result)
        //    System.out.println(s);
        return result;
    }
}
